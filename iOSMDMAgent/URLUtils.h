//
//  URLUtils.h
//  iOSMDMAgent
//


#import <Foundation/Foundation.h>
#import "MDMConstants.h"

@interface URLUtils : NSObject


+ (void)saveServerURL:(NSString *)serverURL;
+ (NSString *)getServerURL;
+ (NSString *)getEnrollmentURL;
+ (NSString *)getContextURL;
+ (NSDictionary *)readEndpoints;
+ (NSString *)getTokenPublishURL;
+ (NSString *)getLocationPublishURL;
+ (NSString *)getUnenrollURL;
+ (NSString *)getRefreshTokenURL;
+ (NSString *)getOperationURL;
+ (void)saveEnrollmentURL:(NSString *)enrollURL;
+ (NSString *)getSavedEnrollmentURL;
+ (NSString *)getEnrollmentURLFromPlist;
+ (NSString *)getServerURLFromPlist;
+ (NSString *)getEffectivePolicyURL;
+ (NSString *)getTokenRefreshURL;
+ (NSString *)getEnrollmentType;
+ (NSString *)getCaDownloadURL;
+ (NSString *)getAuthenticationURL;
+ (NSString *)getLicenseURL;
+ (NSString *)getEnrollURL:(NSString *)tenantDomain username:(NSString *)token;
+ (NSString *)getIsEnrolledURL;

@end
